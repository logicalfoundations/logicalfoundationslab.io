**Exercise 20.** Show that every morphism from a object to an object in a category is an isomorphism.
