**Exercise 38.** Show that functor composition is associative. Use this to conclude that :math:`UK \colon\mathbf{Set}\to\mathbf{Set}` is a functor.

