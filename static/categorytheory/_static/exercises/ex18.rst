**Exercise 18.** In other categories (for example, :math:`\mathbf{Mon}`), find one or more objects such that every object :math:`A` is completely described by
the morphisms from those objects to :math:`A`. What does “completely describes” mean formally?
