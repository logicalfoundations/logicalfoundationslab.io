**Exercise 42 Solution.**

.. math:: \begin{align*} (Ff \circ rev_A)(a_1 a_2  \dots  a_n) &= Ff ( a_n a_{n-1} \dots  a_1)\\ &= map(f)(a_n a_{n-1} \dots  a_1)\\ &= f(a_n) f(a_{n-1}) \dots  f(a_1) \\ &= rev_B \,f(a_1) f(a_{2}) \dots f(a_n)\\ &= rev_B \,map(f) (a_1 a_{2}\dots  a_n)\\ &= (rev_B\circ Ff) (a_1 a_{2} \dots  a_n). \end{align*}