**Exercise 13.** Verify that :math:`\mathcal C^{\mathrm{op}}` is a category whenever :math:`\mathcal C` is a category.
