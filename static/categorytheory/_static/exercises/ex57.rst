**Exercise 57.** Verify that the above definition of "exponential graph" is indeed that of the exponential object in :math:`\mathbf{Grph}`.
