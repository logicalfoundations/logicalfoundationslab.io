-- Example:

variables A B : Type
variable P : A → Prop
variable h : ∀ x, P(x) → B

example (y : A) : P(y) → B := h y
