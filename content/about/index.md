+++
title = "about"
path = "about"
template = "about.html"
date = 2012-01-02
+++

<!-- <div class="bio">
<img src="/images/wjd_nara.jpg" height=625px title="William DeMeo in Nara, Japan" />
<div class="caption"><i>William DeMeo in Nara, Japan</i>
<a href="mailto:williamdemeo@gmail.com"></a>
</div>
</div>-->

### About logicalfoundations.org

Contributors include

+ [Charlotte Aten](mailto:caten2@u.rochester.edu)
+ [Venanzio Capretta](http://www.duplavis.com/venanzio/)
+ [William DeMeo](http://williamdemeo.org)
+ [Hyeyoung Shin](http://hyeyoungshin.org)


[Mathematics Department at University of Colorado, Boulder]: https://math.colorado.edu/
[University of Hawaii]: https://math.hawaii.edu/
[Ralph Freese]: https://math.hawaii.edu/~ralph
[Peter Mayr]: https://math.colorado.edu/~mayr
[Iowa State University]: https://math.iastate.edu/
[Cliff Bergman]: https://orion.math.iastate.edu/cbergman/
[University of South Carolina]: https://sc.edu/study/colleges_schools/artsandsciences/mathematics/index.php
[George McNulty]: https://sc.edu/study/colleges_schools/artsandsciences/mathematics/index.php
[dblp]: https://dblp.uni-trier.de/pers/hd/d/DeMeo:William
[google scholar]: https://scholar.google.com/citations?user=y1OQ07QAAAAJ&hl=en
