+++
template = "page.html"
title = "UACalc"
author = "William DeMeo"
date = "2014-03-10"
---

This page is a guide to some aspects of configuring and using the [Universal Algebra Calculator][] (UACalc). The main web site for UACalc is [uacalc.org][].

Most of the instructions here are aimed at Linux users. If you use 
another operating system, feel free to use these notes as a guide.

-----------------------------------------------------

## Table of Contents

+ [UACalc at the command line](#uacalc-at-the-command-line)
+ [Launching the UACalc GUI](#launching-the-uacalc-gui)
  - [Install Java](#installing-java)
  - [Add UACalc to Exceptions List](#add-uacalc-to-java-security-exceptions-list)
  - [Launch UACalc](#launch-uacalc)

--------------------------------------------
--------------------------------------------

## UACalc at the command line

+ [GitHub repository for the command line version of UACalc](https://github.com/UACalc/UACalc).  
+ [Notes about various ways to use the UACalc from the command](http://universalalgebra.wordpress.com/documentation/uacalc/).  

-------------------------------------------------------
-------------------------------------------------------

## Launching the UACalc GUI

The standard way to use the UACalc is through its graphical user interface.
This requires Java.  There are many ways to get the Java Runtime Environment 
working on a Linux machine, but here we describe how to install the full
Oracle Java Development Kit (JDK).  This is a reasonable option, especially if you 
plan to venture beyond the GUI, and write some Java or Jython or Scala programs 
that call UACalc Java packages.

-------------------------------

### Install Java
Here is one way to install Java on Linux (Ubuntu 13.10). It is not the only way,
but it seems to work.  (Alternative instructions for installing the JDK on Linux
are [here](http://docs.oracle.com/javase/7/docs/webnotes/install/linux/linux-jdk.html).)

1. **Download the Java Development Kit**  

   As of this writing (March, 2014) the latest version of the JDK is 1.7, 
   which is available at the

   [Oracle JDK 7 downloads page](http://www.oracle.com/technetwork/java/javase/downloads/jdk7-downloads-1880260.html).

   For example, I'm now using jdk-7u51-linux-x64.tar.gz, but you should pick the
   tarball that is most appropriate for your hardware. If the link above doesn't
   work, try the following:
   
   [http://www.oracle.com/technetwork/java/javase/downloads/index.html](http://www.oracle.com/technetwork/java/javase/downloads/index.html)

   **UPDATE:**  
   [JDK 8](http://www.oracle.com/technetwork/java/javase/overview/index.html) is
   now available [here](http://www.oracle.com/technetwork/java/javase/overview/index.html).

-------------------------------

2. **Unpack the jdk tarball**  
   Go to the directory where you downloaded the jdk in the previous step 
   and invoke the command 

        tar xvzf jdk-7u*-linux-x64.tar.gz

-------------------------------

3. **Create the jvm directory**

        sudo mkdir -p /usr/lib/jvm
 
-------------------------------

4. **Move the jdk directory**  
   If you already have directory named /usr/lib/jvm/jdk1.7.0, move it out of the
   way:
   
        sudo mv /usr/lib/jvm/jdk1.7.0{,.orig}

   Now move your newly unpacked jdk directory (e.g. jdk1.7.0\_51) to
   /usr/lib/jvm and rename it jdk1.7.0:
   
        sudo mv jdk1.7.0_* /usr/lib/jvm/jdk1.7.0
	
-------------------------------

5. **Make jdk1.7.0 the default Java**  
   We will use the `update-alternatives` program for this
   (see also: [notes on configuring JDK 1.7 on Ubuntu](http://askubuntu.com/questions/55848/how-do-i-install-oracle-java-jdk-7)):

   This first block of 9 commands can be copy-and-pasted to the command line all at once:
		
        sudo update-alternatives --install "/usr/bin/java" "java" "/usr/lib/jvm/jdk1.7.0/bin/java" 1;
        sudo update-alternatives --install "/usr/bin/javac" "javac" "/usr/lib/jvm/jdk1.7.0/bin/javac" 1;
        sudo update-alternatives --install "/usr/bin/javaws" "javaws" "/usr/lib/jvm/jdk1.7.0/bin/javaws" 1;
		sudo update-alternatives --install "/usr/bin/jcontrol" "jcontrol" "/usr/lib/jvm/jdk1.7.0/bin/jcontrol" 1;
        sudo chmod a+x /usr/bin/java;
        sudo chmod a+x /usr/bin/javac;
        sudo chmod a+x /usr/bin/javaws;
        sudo chmod a+x /usr/bin/jcontrol;
        sudo chown -R root:root /usr/lib/jvm/jdk1.7.0;

   The following commands are interactive and should be invoked individually:
		
        sudo update-alternatives --config java
        sudo update-alternatives --config javac
        sudo update-alternatives --config javaws
        sudo update-alternatives --config jcontrol

   (Note: the `jcontrol` command is not mentioned in 
   [the notes cited above](http://askubuntu.com/questions/55848/how-do-i-install-oracle-java-jdk-7), 
   but we will need it below.)
   
You can check which version of Java your system is currently using with the command`java -version`.


------------------------------------------------------

### Add UACalc to Java Security Exceptions List

(As of March 2014, the Java security certificate for the UACalc has been
renewed, so it shouldn't be necessary to follow all of the steps in this
section. After installing Java as described above, and then following steps 1
and 2 below, the UACalc gui should run fine. However, I'll leave the information
in this section as is, in case Ralph decides it's not worth renewing the
security certificate in the future.)

In an ideal world, assuming you successfully installed Java as described in 
the previous step, you would now be able to go to [uacalc.org][] 
and click a `Launch` button.  However, the world is not idea, and launching
UACalc for the first time now requires an extra step.
We must first tell Java that we trust the site www.uacalc.org.
(This used to be a simple matter of checking a box, but Oracle has 
recently made the procedure for accepting security certificates even 
more annoying.)

1. **Get the uacalc.jnlp file**  
   Go to [uacalc.org][] and download the uacalc.jnlp file that is most 
   appropriate for your hardware.  For example, if your machine has 8Gb 
   of RAM, you probably want
   [uacalcbig8.jnlp](http://www.uacalc.org/uacalcbig8.jnlp).


------------------------------------------------------

2. **Try to launch the UACalc gui** (and probably fail)  
   In a terminal window, go to the directory where you downloaded the file 
   in the previous step and try to launch UACalc with the following 
   command:

        javaws uacalcbig4.jnlp

   If UACalc starts up, you're good to go!  More than likely, however,
   you will get an annoying dialog box like the following:
   
   <img src="{{ root_url }}/images/ApplicationBlocked.png" />
   
   Where is the checkbox on this dialog so that I can accept the risk and 
   proceed at my own peril?  It's gone. So we have no choice but to
   select the `OK` button to abort launch and follow the steps below.

------------------------------------------------------

3. **Launch the Java Control Panel**  
   At the command line, type `jcontrol`
   You should see a window that looks like this:

   <img src="{{ root_url }}/images/JavaControlPanel.png" />
   
   If you get an error, try typing `/usr/lib/jvm/jdk1.7.0/bin/jcontrol`
   
------------------------------------------------------

4. **Add uacalc.org to the Exception Site List**  
   Click on the `Security` tab, and click the `Edit Site List` button. 
   You should see a dialog box that looks like this:
   
   <img src="{{ root_url }}/images/ExceptionSiteList.png" />
   
   Click the `Add` button and type http://www.uacalc.org and click `OK`.

   You will get a warning.  Click `Continue`.  
   
   If your Java Control Panel now looks like the one below, click `OK`.
   
   <img src="{{ root_url }}/images/JavaControlPanelAfter.png" />
   
   
------------------------------------------------------

### Launch UACalc
   Now, when you invoke 

        javaws ~/Desktop/uacalc/uacalcbig4.jnlp
   
   at the command line, you should see a less futile and pointless window 
   than the one we saw in Step 2.
   
   <img src="{{ root_url }}/images/JavaSecurityWarning.png" />
   
   Accept the risks and click `OK` and you should finally see the 
   UACalc gui, which looks like this:

   <img src="{{ root_url }}/images/UACalcInitialView.png" />
   

[uacalc.org]: http://www.uacalc.org
[Universal Algebra Calculator]: http://www.uacalc.org
