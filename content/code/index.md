+++
title = "code"
path = "code"
template = "page.html"
date = 2019-03-12
+++

...where we will collect links to software resources...

**This page is currently under development.**


<!-- more -->

+ [lean-ualib](./lean-ualib/index.md)  

--------------------------------

The [TypeFunc](https://github.com/williamdemeo/TypeFunc) list of resources (about type theory, functional programming, and related subjects) is my most popular github repository.
