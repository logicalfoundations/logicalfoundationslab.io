+++
template = "page.html"
title = "scala"
date = 2014-03-13
+++

## Scala Coursera Courses

I took two Coursera courses about Scala,

+ [Functional Programming Principles in Scala](https://www.coursera.org/course/progfun)
by Martin Odersky.


+ [Principles of Reactive Programming](https://www.coursera.org/course/reactive)
by Martin Odersky, Erik Meijer, Roland Kuhn.

The pages [progfun]({{ root_url }}/scala/progfun) and
[reactive]({{ root_url }}/scala/reactive) collect some of my own notes about
these courses, and are intended for my own future reference.

