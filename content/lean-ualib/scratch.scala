def f(x : 1 + ℕ + (ℕ × ℕ)) : 
| in₀ 1 := e
| in₁ x := x⁻¹
| in₂ x y := x ∘ y

