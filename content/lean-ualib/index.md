+++
title = "lean-ualib"
description = "Documentation for background theory underlying the lean-ualib"
date = 2019-03-13
[extra]
banner="composition"
+++
(Shown above is a general formulation of composition of operations in dependent type theory, as derived [here](composition).)

This page has moved to [williamdemeo.org/lean-ualib](https://williamdemeo.gitlab.io/lean-ualib/).

