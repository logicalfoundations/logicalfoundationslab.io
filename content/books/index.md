+++
title = "books"
description = "posts related to books"
path = "books"
template = "page.html"
date = 2019-03-13
+++

...where we will collect links to relevant books...

**This page is currently under development.**

+ [Category Theory: a concise course](https://categorytheory.gitlab.io/)

+ [The Lean Universal Algebra Library](https://ualib.gitlab.io)

<!-- more -->

